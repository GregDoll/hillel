package hillel;

import java.util.Arrays;
import java.util.Random;

public class AlgorithmRunner {

    public static void main(String[] args) {


     int size = 1000;
     int lowBound = -200;
     int highBound = 250;

        int [] array = new int[size];

        Random random = new Random();
        for (int i = 0; i < array.length ; i++) {
            array[i] = random.nextInt(highBound - lowBound) + lowBound;
        }
//

//
//
//        //Finding min and max
//
//        int min = array[0];
//        int max = array[1];
//
//        if (min > max){
//            int temp = min;
//            min = max;
//            max = temp;
//        }
//
//        for (int i = 0; i < array.length; i++) {
//
//            if (array[i] >= max){
//                max = array[i];
//            }
//
//            if (array[i] <= min){
//                min = array[i];
//            }
//
//
//        }
//        System.out.println(Arrays.toString(array));
//        System.out.println("Min= " + min);
//        System.out.println("Max= " + max);
//        long start = System.currentTimeMillis();
//        int number = 249;
//        int numIndex = 0;
//
//        for (int i = 0; i < array.length ; i++) {
//            if (array[i] == number){
//                numIndex = i;
//                break;
//
//            }
//
//        }
//        long end =  System.currentTimeMillis();
//
////        System.out.println(Arrays.toString(array));
//        if (numIndex <0) {
//            System.out.println("Number" + number + " found on position " + numIndex);
//        } else {
//            System.out.println("Number " + number + " found on position " + numIndex);
//        }
//        System.out.println("Array processed in " + (end - start));

        long startBubble = System.currentTimeMillis();
        System.out.println(Arrays.toString(array));
        //Bubble sort algorithm
        for (int i = 0; i < array.length ; i++) {

            for (int j = 1; j < array.length - i ; j++) {

                if(array[j] < array[j - 1]){

                    int temp = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = temp;

                }


            }

        }
        long endBubble = System.currentTimeMillis();

        System.out.println("Sorted by bubble in " + (endBubble - startBubble));
        System.out.println(Arrays.toString(array));



    }

}
