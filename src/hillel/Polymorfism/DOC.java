package hillel.Polymorfism;

public class DOC extends NPC {

    @Override
    public void speak() {
        System.out.println("Nurse! I need blood packet!");
    }

    @Override
    public void move(int x, int y) {
        System.out.println("Ok! Moving to position!");
        super.move(x, y);
    }

    @Override
    public void action(NPC npc) {
        int health =  npc.getHealth();
        if (health < 100){
            npc.setHealth(100);
        }else System.out.println("You're healthy as a bull!");
    }

    public  void getNPCsHealth()
}
