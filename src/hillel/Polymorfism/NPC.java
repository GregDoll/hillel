package hillel.Polymorfism;

import java.util.Objects;

public class NPC {

    private int health;
    private int x;
    private int y;


    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public NPC(int health, int x, int y) {
        this.health = health;
        this.x = x;
        this.y = y;
    }

    public NPC() {
        this.health = 100;
    }

    @Override
    public String toString() {
        return "NPC{" +
                "health=" + health +
                ", x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NPC npc = (NPC) o;
        return health == npc.health &&
                x == npc.x &&
                y == npc.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(health, x, y);
    }

    public void speak(){
        System.out.println("Blah-blah!");
    }

    public void move(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void action(NPC npc){
        System.out.println("Ahaaaaa!");
    }

}
