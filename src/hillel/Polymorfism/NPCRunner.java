package hillel.Polymorfism;

import java.util.Objects;

public class NPCRunner {

    public static void main(String[] args) {

//        NPC npc = new NPC();
//
//        npc.speak();
//        npc.move(0,3);
//        npc.action(new NPC());

        NPC n1 = new DOC();
        NPC n2 = new Private();
        NPC n3 = new Officer();

        NPC [] npcs = {n1, n2, n3};

        for (NPC n : npcs) {
            System.out.println(n);
            n.speak();
            n.move(0,0);
            n.action(new NPC());
            System.out.println();
        }

    }
}
