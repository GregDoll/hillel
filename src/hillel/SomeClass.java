package hillel;

import java.util.Random;

public class SomeClass {
    public static void main(String[] args) {

        int a = 1;

///*
//         System.out.println(Integer.toBinaryString(a));
//
//         System.out.println(Integer.toBinaryString(~a));
//
//         System.out.println(!false);
//
//
//         System.out.println(4*2);
//         System.out.println(4/2);
//         System.out.println(4%2);
//         System.out.println(4+7);
//         System.out.println(2 - 11);
//
//         System.out.println(4.0f/5.0f);
//*/
        int b = 2;
        System.out.println(a != b);
        System.out.println(true || false);

        System.out.println(true && false);

        b = a % 2 != 0 ? 5 : 17;

//        { a += 2;
//         a = a + 2; the same }
        double result = Math.pow(25, 2);
        double res = Math.abs(-265);
        System.out.println(result);
        System.out.println(res);
        System.out.println(Math.sqrt(25));
        double random = Math.random();
        System.out.println(random);

        System.out.println((int) (random * 1000));

        Random otherRandom = new Random();
        System.out.println(otherRandom.nextInt(10));

        System.out.println();

    }
}
