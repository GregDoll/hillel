package hillel;

import java.util.Random;

public class Test {

    public static void main(String[] args){

//        Test2 test2 = new Test2();
//        Test2 test3 = new Test2("Sample text");
//
//        double result = test2.divide(3,7);
//
//        System.out.println(result);

        Point pointA = new Point(3.5, 2.8);
        Point pointB = new Point(0, 10);

        double distance =  pointA.distanceTo(pointB);

        System.out.println("Distance between points is: " + distance);
        int number = 5;

        int num = 10;
        System.out.println("Factorial of " + number + " is " + fact(num));
        fact(10);
    }

    public static long factorial(int number){
        if (number == 0) return 1;
        long result = 1;
        for (int i = number; i >= 1 ; i--) {
            result *= i;
        }
        return result;
    }

    public static long fact (int number){
        if (number == 0) return 1;
        return number * fact(number - 1);
    }

}
