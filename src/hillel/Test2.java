package hillel;

public class Test2 {
    int number = 2;

    public Test2(){
        System.out.println("Default");
    }

    public Test2(String text){
        System.out.println(text);
    }

    public int divide (int number){
        return number / this.number;
    }

    public double divide(int a, int b) {
        return (double) a/b;
    }
    public int divide (double a, int b){
        return (int) a/b;
    }
    public int divide (int a, double b){
        return a / (int) b;
    }
}
