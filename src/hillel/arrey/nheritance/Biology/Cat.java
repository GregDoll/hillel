package hillel.arrey.nheritance.Biology;

import hillel.arrey.nheritance.Biology.Mammal;

public class Cat extends Mammal {

    public Cat() {
        super();
        System.out.println("Cat has been created");
    }

    public void miao(){
        System.out.println("Miao!");
    }
}
