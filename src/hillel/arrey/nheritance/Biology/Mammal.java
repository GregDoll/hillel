package hillel.arrey.nheritance.Biology;

public class Mammal extends Animals {

    public Mammal(){
        super();
        System.out.println("Mammal object has been created");
    }

    public void drinkMilk(){
        System.out.println("I drink milk when I small");
    }

    public void blood(){
        System.out.println("My blood is warm");
    }

}

