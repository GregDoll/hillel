package hillel.arrey.nheritance.Biology.persons;

public class Citizen extends Person {

    private String country;

    private long personalNumber;

    private Passport passport;

    public Citizen() {
    }

    public Citizen(String sex, String name, String secondName, int age, String country, long personalNumber, Passport passport) {
        super(sex, name, secondName, age);
        this.country = country;
        this.personalNumber = personalNumber;
        this.passport = passport;
        passport.setName(getName());
        passport.setSecondName(getSecondName());
    }

    public Citizen(String sex, String name, String secondName, int age) {
        super(sex, name, secondName, age);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(long personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Passport getPassport() {
        return passport;
    }

    public void setPassport(Passport passport) {
        passport.setName(getName());
        passport.setSecondName(getSecondName());
        this.passport = passport;
    }

    @Override
    public String toString() {
        return super.toString() + "=" + "Citizen{" +
                "country='" + country + '\'' +
                ", personalNumber=" + personalNumber +
                ", passport=" + passport +
                '}';
    }
}
