package hillel.arrey.nheritance.Biology.persons;

import java.time.LocalDate;

public class Passport {

    private LocalDate dateOfIssuing;

    private String name;
    private String secondName;

    private String number;

    public Passport(LocalDate dateOfIssuing, String name, String secondName, String number) {
        this.dateOfIssuing = dateOfIssuing;
        this.name = name;
        this.secondName = secondName;
        this.number = number;
    }

    public Passport() {
    }

    public LocalDate getDateOfIssuing() {
        return dateOfIssuing;
    }

    public void setDateOfIssuing(LocalDate dateOfIssuing) {
        this.dateOfIssuing = dateOfIssuing;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return  "Passport{" +
                "dateOfIssuing=" + dateOfIssuing +
                ", name='" + name + '\'' +
                ", secondName='" + secondName + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
