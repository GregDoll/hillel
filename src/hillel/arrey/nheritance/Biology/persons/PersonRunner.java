package hillel.arrey.nheritance.Biology.persons;

import java.time.LocalDate;

public class PersonRunner {
    public static void main(String[] args) {

        Citizen citizen = new Citizen();
// Ниже представлен обект
        citizen.setPersonalNumber(325897564);
        citizen.setCountry("Ukraine");
        citizen.setAge(16);
        citizen.setSex("combat helicopter");
        citizen.setName("Bubble");
        citizen.setSecondName("Bubbleson");
        Passport passport = new Passport();

        passport.setNumber("HH28170");
        passport.setDateOfIssuing(LocalDate.of(2020, 1, 2));
        citizen.setPassport(passport);

        System.out.println(citizen);
//Конец обекта
    }
}
